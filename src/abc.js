export const toBooleanBinary = data => {
  if (!data || data === 'no') {
    return 0
  }

  return 1
}

export const FUNCTION_IDENTITY = i => i

export const conv = (data, options = {}) => {
  const defaultOptions = {
    joinWith: ',',
    dataTransformer: FUNCTION_IDENTITY,
  }

  const mergedOptions = Object.assign({}, defaultOptions, options)

  return Object.entries(data)
    .map(([key, value]) => `${key}=${mergedOptions.dataTransformer(value)}`)
    .join(mergedOptions.joinWith)
}
