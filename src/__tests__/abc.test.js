import { conv, toBooleanBinary } from '../abc'

describe('specs', () => {
  const transform = val =>
    Number.isNaN(val) || typeof val !== 'number' ? toBooleanBinary(val) : val
  const specs = [
    {
      input: { width: 10, height: 30 },
      expect: 'width=10,height=30',
      options: { dataTransformer: transform },
    },
    {
      input: { width: 50 },
      expect: 'width=50',
      options: { dataTransformer: transform },
    },
    {
      input: { width: 20, height: 40 },
      expect: 'width=20&height=40',
      options: { joinWith: '&', dataTransformer: transform },
    },
    {
      input: { resizable: 'no' },
      expect: 'resizable=0',
      options: { dataTransformer: transform },
    },
    {
      input: { resizable: 'no', abc: 'no' },
      expect: 'resizable=0,abc=0',
      options: { dataTransformer: transform },
    },
  ]
  specs.forEach(spec => {
    it(`should return ${spec.expect} when passing ${JSON.stringify({
      ...spec.input,
      options: spec.options,
    })}`, () => {
      expect(conv(spec.input, spec.options)).toEqual(spec.expect)
    })
  })
})

describe('toBooleanBinary', () => {
  const specs = [
    {
      input: 'no',
      expect: 0,
    },
    {
      input: 'yes',
      expect: 1,
    },
    {
      input: 'qwe',
      expect: 1,
    },
    {
      input: {},
      expect: 1,
    },
    {
      input: [],
      expect: 1,
    },
    {
      input: undefined,
      expect: 0,
    },
    {
      input: null,
      expect: 0,
    },
    {
      input: NaN,
      expect: 0,
    },
    {
      input: 0,
      expect: 0,
    },
    {
      input: false,
      expect: 0,
    },
    {
      input: '',
      expect: 0,
    },
  ]
  specs.forEach(spec => {
    it(`should return ${spec.expect} when passing ${JSON.stringify(spec.input)}`, () => {
      expect(toBooleanBinary(spec.input)).toEqual(spec.expect)
    })
  })
})
